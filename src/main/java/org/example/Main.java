package org.example;


import lombok.extern.slf4j.Slf4j;
import org.example.api.SearchRequest;
import org.example.data.Flat;

import org.example.manager.FlatManager;

import java.util.List;

@Slf4j
public class Main {
    public static void main(String[] args) {
        Flat flat1 = new Flat(0, "studio", 2_000_001, 30, true, false, 4, 5, false);
        Flat flat2 = new Flat(0, "1", 2_000_002, 20, false, true, 5, 9, false);
        Flat flat3 = new Flat(0, "2", 2_000_000, 32, true, false, 3, 19, false);
        Flat flat4 = new Flat(0, "3", 2_000_001, 30, true, false, 1, 5, false);
        Flat flat5 = new Flat(0, "4", 2_000_001, 30, true, false, 5, 5, false);
        Flat flat6 = new Flat(0, "5", 2_000_001, 30, true, false, 1, 5, false);
        Flat flat7 = new Flat(0, "6", 2_000_001, 30, true, false, 5, 5, false);
        Flat flat8 = new Flat(0, "free_style", 2_000_001, 30, true, false, 5, 5, false);

        SearchRequest search = new SearchRequest(new String[]{"2"}, 1_000_000, 5_000_000, 20, 100,
                true, false, 2, 5, 2, 19, false, false);

        SearchRequest search1 = new SearchRequest(new String[]{"2", "5"}, 1_000_000, 5_000_000, 20, 100,
                true, false, 2, 5, 2, 19, false, false);

        FlatManager flatManager = new FlatManager();
        flatManager.create(flat1);
        flatManager.create(flat2);
        flatManager.create(flat3);
        flatManager.create(flat4);
        flatManager.create(flat5);
        flatManager.create(flat6);
        flatManager.create(flat7);
        flatManager.create(flat8);

        List<Flat> result = flatManager.searchBy(search);
        log.debug("searchBy, result: {}", result + "\n");

        List<Flat> result1 = flatManager.searchBy(search1);
        log.debug("searchBy, result1: {}", result1 + "\n");

        int count = flatManager.getCount();
        log.debug("getCount, count: {}", count + "\n");
    }
}
